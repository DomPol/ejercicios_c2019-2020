/*
    printf(""); --> Imprime por consola un texto.    
    
    Print variable types:
        %i o %d --> int (signed integer)
        %c      --> char (character value)
        %f      --> float (floating point value)
        %s      --> string (characters, words)
        
    Special characters
        \n      --> newline
        \t      --> tab
        \b      --> backspace
        \r      --> carriage return
        
    Example:
        
        int main()
        {
            int number;
            number = 6;
            
            printf("The number is %i \n", number);
            
            return 0;
        }
        
        output: The number is 6
*/

#include <stdio.h>

int main()
{
    printf("New line \n");
    getchar();
    printf("Tab \t");
    getchar();
    printf("BackSpace \b");
    getchar();
    printf("carriage return \r");
    getchar();
    
    printf("Integer %i", 2);
    getchar();
    printf("Integer %d", 2);
    getchar();
    printf("Character %c", 'a');
    getchar();
    printf("Float %f", 2.2F);
    getchar();
    printf("String %s", "word");
    getchar();
    return 0;
}
